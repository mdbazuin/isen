import app
import signal
import sys
import RPi.GPIO as GPIO


def cleanup(signal, frame):
    GPIO.cleanup()
    print("Now exiting")
    sys.exit(0)

if __name__ == '__main__':
    signal.signal(signal.SIGINT, cleanup)
    app.create_app().run(host='0.0.0.0')
