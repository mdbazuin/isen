from app.regulators.Regulator import Regulator


class CoffeeStrengthRegulator(Regulator):
    def set_strength(self, strength):
        self.write(strength)
