from app.regulators.Regulator import Regulator


class CoffeeVolumeRegulator(Regulator):
    def set_volume(self, volume):
        self.write(volume)
