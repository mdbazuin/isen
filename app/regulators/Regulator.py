import RPi.GPIO as GPIO
import time


class Regulator:
    def __init__(self, io_pin=0, default_value=0, is_pwm=False, inverted=False):
        self.io_pin = io_pin
        self.default_value = default_value
        self.is_pwm = False
        self.inverted = inverted
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.io_pin, GPIO.OUT, initial=GPIO.HIGH)

    def write(self, value):
        if self.is_pwm:
            GPIO.PWM(self.io_pin, value)
            return

        GPIO.output(self.io_pin, value)

    def off(self):
        if self.is_pwm:
            GPIO.PWM(self.io_pin, 0)
            return
        if self.inverted:
            GPIO.output(self.io_pin, GPIO.LOW)
            return

        GPIO.output(self.io_pin, GPIO.HIGH)

    def on(self):
        if self.is_pwm:
            GPIO.PWM(self.io_pin, self.default_value)
            return

        if self.inverted:
            GPIO.output(self.io_pin, GPIO.HIGH)
            return

        GPIO.output(self.io_pin, GPIO.LOW)

    def simulate_button_press(self):
        if not self.is_pwm:
            GPIO.output(self.io_pin, GPIO.LOW)
            time.sleep(0.5)
            GPIO.output(self.io_pin, GPIO.HIGH)
            return True

        # If The regulator is a pwm regulator
        # Do not do the button press
        # And return false
        return False
