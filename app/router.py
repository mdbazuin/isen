from app.common.BaseRouter import BaseRouter
from app.api import ApiRouter


class AppRouter(BaseRouter):
    """Main router based on the BaseRouter"""
    child_routers = [
        ApiRouter
    ]

    def routes(self):
        # Add route for the base_path of the router
        @self.app.route(self.base_path)
        def home():
            return {'message': 'welcome'}
