from app.regulators import Regulator
from app.receivers import Receiver
import RPi.GPIO as GPIO
from datetime import datetime
import time
import threading


class PlatformRotator:
    def __init__(self, read_pin, out_pin, pwm_pin):
        self.receiver = Receiver(read_pin,
                                 callback=self.__event_callback,
                                 falling=True)
        self.out_regulator = Regulator(io_pin=out_pin, inverted=True)
        self.out_regulator.off()
        self.pwm_regulator = Regulator(io_pin=pwm_pin,
                                       is_pwm=True)

        self.__is_rotating = False
        self.started_rotating_at = None
        self.lock = threading.Lock()

    def __event_callback(self, x):
        # self.lock.acquire()
        if self.started_rotating_at is not None:
            time_diff = (datetime.now() - self.started_rotating_at)
            time_diff = time_diff.total_seconds() * 1000.0
        else:
            time_diff = 0
        if self.__is_rotating and time_diff > 600:
            self.out_regulator.off()
            self.started_rotating_at = None
            self.__is_rotating = False

        # self.lock.release()

    def rotate(self):
        self.__is_rotating = True
        self.started_rotating_at = datetime.now()
        self.out_regulator.on()

    def is_rotating(self):
        return self.__is_rotating
