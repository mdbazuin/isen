class ADSensor:
    def __init__(self, channel, decimal_numbers):
        self.channel = channel
        self.decimal_numbers = decimal_numbers
        self.volts = 0

    def convert_volts(self, data):
        volts = (data * 3.3) / float(1023)
        self.volts = round(volts, self.decimal_numbers)

    def convert_data(self, data):
        self.convert_volts(data)
