import spidev
import time
import threading


class ADConverter:
    def __init__(self, delay=5):
        self.sensors = []
        self.delay = delay
        self.spi = spidev.SpiDev()
        self.spi.open(0, 0)

    def adjust_delay(self, delay):
        self.delay = delay

    def add_sensor(self, sensor):
        self.sensors.append(sensor)

    def read_channel(self, channel):
        adc = self.spi.xfer2([1, (8 + channel) << 4, 0])
        data = ((adc[1] & 3) << 8) + adc[2]
        return data

    def read(self):
        for sensor in self.sensors:
            try:
                data = self.read_channel(sensor.channel)
                sensor.convert_data(data)
            except AttributeError as e:
                print(e)

        time.sleep(self.delay)

    def start_reading(self):
        def thread(parent):
            while True:
                parent.read()

        t = threading.Thread(target=thread, args=(self,))
        t.start()
