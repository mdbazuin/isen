from .ADSensor import ADSensor


class TemperatureSensor(ADSensor):
    def __init__(self, channel, decimal_numbers):
        super().__init__(channel, decimal_numbers)
        self.temperature = 0

    def convert_temperature(self, data):
        # ADC Value
        # (approx)  Temp  Volts
        #  124        0    0.40
        #  230       17    0.74
        #  335       34    1.08
        #  440       51    1.42
        #  546       68    1.76
        #  651       85    2.10
        #  757      102    2.44
        # 1023      125    3.30

        temp = ((data-124)/float(1023))*125
        self.temperature = round(temp, self.decimal_numbers)

    def convert_data(self, data):
        super().convert_data(data)
        self.convert_temperature(data)