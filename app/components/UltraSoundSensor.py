from app.regulators import Regulator
from app.receivers import Receiver
import time
import threading


class UltraSoundSensor:
    def __init__(self, trigger, echo):
        self.trigger_regulator = Regulator(io_pin=trigger, inverted=True)
        self.trigger_regulator.off()

        self.echo_reciever = Receiver(echo, with_callback=False, pull_up=True)
        self.lock = threading.Lock()
        self.distance = 0

    def read(self):
        self.trigger_regulator.off()
        time.sleep(0.05)

        self.trigger_regulator.on()
        time.sleep(0.00001)
        self.trigger_regulator.off()

        pulse_start = time.time()
        pulse_end = time.time()

        while self.echo_reciever.get_current_value() == 0:
            pulse_start = time.time()

        while self.echo_reciever.get_current_value() == 1:
            pulse_end = time.time()

        # Using 17150 as 343m/s is speed for sound
        # 34300cm/s
        # 34300/2 = 17150
        pulse_duration = pulse_end - pulse_start
        # distance = pulse_duration / 0.00000295 / 2 / 10
        distance = pulse_duration * 17150

        self.lock.acquire()
        self.distance = round(distance, 2)
        self.lock.release()

    def start_reading(self):
        def thread(parent):
            while True:
                parent.read()
                time.sleep(1)

        t = threading.Thread(target=thread, args=(self,))
        t.start()
