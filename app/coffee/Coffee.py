from app.coffee_machine import CoffeeMachine
import time
import RPi.GPIO as GPIO
import app.common.constants.status as status


class Coffee:

    def __init__(self, machine, volume=0.0, coffee_strength=0.0):
        self.__volume = volume
        self.__coffee_strength = coffee_strength

        if isinstance(machine, CoffeeMachine):
            self.__machine = machine
        else:
            self.__machine = None

    def get_machine():
        return self.__machine

    def get_volume():
        return self.__volume

    def get_coffee_strength():
        return self.__coffee_strength

    def brew(self, cups=1, strong=False, test=False):
        # Check if the machine exists in the class
        if self.__machine is None:
            # If not return immediately
            return 'Machine isn\'t valid'

        # Set the target to the amount of cups
        self.__machine.set_brewing_target(cups)

        # Reset the progress to 0
        self.__machine.set_currently_brewing_cup(0)

        # Set if the coffee is strong or not
        self.__machine.set_brewing_strong(strong)

        print("Toggling power to turn it on")
        # Set status of turning on,
        # so we can see the status of the machine in the api
        self.__machine.set_status(status.POWERING_ON)

        # Turn the machine on
        self.__machine.toggle_power()

        # Set status of heating,
        # so we can see the status of the machine in the api
        self.__machine.set_status(status.HEATING)

        # Add a small delay,
        # so the coffee machine always has time to initialize if needed
        time.sleep(10)

        if not test:
            # If we are not running a test
            # Wait for the machine to have a minimum boiler temperature
            # of at least 70.0 degrees Celcius
            while not self.__machine.get_boiler_temperature() >= 70.0:
                time.sleep(1)

        # Following code is commented,
        # because we can't control both of the following regulators anymore
        # due to unforseen hardware issues

        # Set the Coffee strength with the coffee strength regulator
        # from the machine
        # cs_regulator = self.__machine.get_coffee_strength_regulator()
        # cs_regulator.set_strength(self.__coffee_strength)

        # Set the coffee volume with the coffee volume regulator
        # from the machine
        # cv_regulator = self.__machine.get_coffee_volume_regulator()
        # cv_regulator.set_volume(GPIO.HIGH)
        # time.sleep(5)
        # cv_regulator.set_volume(GPIO.LOW)

        print("Start brewing x cups of coffee")

        # Start a loop to brew the cups of coffee as given in the cups variable
        for i in range(cups):
            # Set status of brewing,
            # so we can see the status of the machine in the api
            self.__machine.set_status(status.BREWING)

            # Set progress to current cup
            self.__machine.set_currently_brewing_cup(i+1)
            
            if not test:
                # If we are not running a test
                # Brew coffee
                self.__machine.start_brewing()
                print("Wait 60 seconds")
                
                # Wait 60 seconds for coffee to finish brewing
                time.sleep(60)
            else:
                # If we are testing the system
                # Do not brew coffee
                # And wait 6 seconds.
                time.sleep(6)
            
            # Set status of turning the platform,
            # so we can see the status of the machine in the api
            self.__machine.set_status(status.TURNING_PLATFORM)

            # Tell the machine to turn the platform
            self.__machine.turn_platform()

            # Add a small delay, before continuing the loop
            time.sleep(1)

        print("Toggling power to turn it off")

        # Set status of turning on,
        # so we can see the status of the machine in the api
        self.__machine.set_status(status.POWERING_OFF)

        # Turn off the machine
        self.__machine.toggle_power()

        # Set status of idle,
        # so we can see the status of the machine in the api
        self.__machine.set_status(status.IDLE)

        # Reset the brewing strong to default False
        self.__machine.set_brewing_strong(False)

        # return so we can let everyone know the function succeeded
        return 'success'
