from app.common.BaseRouter import BaseRouter
from .v1 import V1Router


class ApiRouter(BaseRouter):
    child_routers = [
        V1Router
    ]

    relative_path = '/api'

    def routes(self):
        @self.app.route(self.base_path)
        def api():
            return {'message': 'api'}
