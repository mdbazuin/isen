from app.common.BaseRouter import BaseRouter
from flask import request
from .handlers import brew_coffee_handler


class CoffeeAPIRouter(BaseRouter):
    child_routers = []
    relative_path = '/coffee'

    def routes(self):
        @self.app.route(self.base_path+"/brew", methods=["POST"])
        def brew():
            return brew_coffee_handler(request.data)
