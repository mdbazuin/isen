from app.coffee.Coffee import Coffee
from app.coffee_machine import global_machine
import json
import threading


def brew_coffee_handler(data):
    # Check if the machine is in idle status
    if not global_machine.is_idle():
        # If not, let the user know
        return {
            'msg': 'coffee machine is busy',
            'status': global_machine.get_status(),
            'started_brewing': False
        }

    # Default 1 cup of coffee
    cups = 1

    if 'cups' in data:
        # Unless the cups variable has been given in the request
        cups = data['cups']

    # Default normal coffee
    strong = False

    if 'is_strong' in data:
        # Unless the is_strong variable has been given in the request
        strong = data['is_strong']

    # Default not testing
    test = False

    if 'test' in data:
        # Unless the test variable has been given in the request
        test = data['test']

    # Create a function, which we can run in a seperate thread
    def thread(cups):
        coffee = Coffee(global_machine)
        coffee.brew(cups=cups, strong=strong, test=test)
    
    # Create the thread
    t = threading.Thread(target=thread, args=(cups,))

    # Run the thread.
    t.start()

    # Return that brewing has started
    return {
        'msg': 'Started brewing {} cups of coffee'.format(cups),
        'status': global_machine.get_status(),
        'started_brewing': True
    }
