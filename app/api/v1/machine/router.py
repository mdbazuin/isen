from app.common.BaseRouter import BaseRouter
from flask import request
from .handlers import status_handler


class MachineAPIRouter(BaseRouter):
    child_routers = []
    relative_path = '/machine'

    def routes(self):
        @self.app.route(self.base_path+"/status", methods=["GET"])
        def status():
            return status_handler(request)
