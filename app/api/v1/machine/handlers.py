from app.coffee_machine import global_machine


def status_handler(request):
    return {
        'sensors': {
            'boiler_temperature': global_machine.get_boiler_temperature(),
            'water_height': global_machine.get_water_height(),
            'light_sensor': global_machine.get_light_sensor_value()
        },
        'status': global_machine.get_status(),
        'is_idle': global_machine.is_idle(),
        'brewing': {
            'target': global_machine.get_brewing_target(),
            'current_cup': global_machine.get_currently_brewing_cup(),
            'is_strong': global_machine.get_brewing_strong()
        }
    }
