def shutdown_handler(request):
    if request is None:
        raise RuntimeError('request is none')
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with a Werkzeug server')
    func()
