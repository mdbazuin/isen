from app.common.BaseRouter import BaseRouter
from flask import request
from .handlers import shutdown_handler
from .test import SystemTestAPIRouter


class SystemAPIRouter(BaseRouter):
    child_routers = [
        SystemTestAPIRouter
    ]
    relative_path = '/system'

    def routes(self):
        @self.app.route(self.base_path+"/shutdown", methods=["POST"])
        def shutdown():
            return shutdown_handler(request)
