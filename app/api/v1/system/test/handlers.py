from app.coffee_machine import global_machine
import time
import threading


def rotate_platform_handler(request):
    rotations = 1
    data = request.data
    if data['rotations']:
        rotations = data['rotations']

    global_machine.turn_platform(steps=rotations)
    return {'msg': 'turning {} times'.format(rotations)}


def power_cycle_handler(request):
    global_machine.toggle_power()
    time.sleep(0.5)
    global_machine.toggle_power()
    return {'msg': 'toggled power twice'}


def coffee_button_handler(request):
    global_machine.toggle_coffee_button()
    return {'msg': 'toggled coffee button'}


