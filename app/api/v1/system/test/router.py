from app.common.BaseRouter import BaseRouter
from flask import request
from .handlers import (rotate_platform_handler,
                       power_cycle_handler,
                       coffee_button_handler)


class SystemTestAPIRouter(BaseRouter):
    child_routers = []
    relative_path = '/test'

    def routes(self):
        @self.app.route(self.base_path+"/rotate_platform", methods=["POST"])
        def rotate_platform():
            return rotate_platform_handler(request)

        @self.app.route(self.base_path+"/power_cycle", methods=["POST"])
        def power_cycle():
            return power_cycle_handler(request)

        @self.app.route(self.base_path+"/coffee_button", methods=["POST"])
        def coffee_button():
            return coffee_button_handler(request)
