from app.common.BaseRouter import BaseRouter
from .coffee.router import CoffeeAPIRouter
from .system import SystemAPIRouter
from .machine import MachineAPIRouter


class V1Router(BaseRouter):
    child_routers = [
        CoffeeAPIRouter,
        MachineAPIRouter,
        SystemAPIRouter
    ]
    relative_path = '/v1'

    def routes(self):
        @self.app.route(self.base_path)
        def v1():
            return {'message': 'v1'}
