from app.regulators import (CoffeeStrengthRegulator,
                            CoffeeVolumeRegulator,
                            Regulator)
from app.components import (PlatformRotator,
                            ADConverter,
                            ADSensor,
                            TemperatureSensor,
                            UltraSoundSensor)
import app.common.constants.status as status
import RPi.GPIO as GPIO
import time


class CoffeeMachine:

    # Define constants for regulating the strength and volume of a cup of cofee
    __cs_regulator = None  # Coffee Strength Regulator
    __cv_regulator = None  # Coffee Volume Regulator
    __power_button_regulator = None
    __coffee_button_regulator = None
    __scb_regulator = None  # Strong Coffee Regulator
    __cleaning_button_regulator = None
    __platform_rotator = None
    __ad_converter = None
    __light_sensor = None
    __boiler_sensor = None
    __water_height_sensor = None

    def __init__(self,
                 power_io_pin,
                 coffee_io_pin,
                 strong_coffee_io_pin,
                 cleaning_io_pin):
        # Initizalize the Coffee strength regulator,
        # Although this will not be used due to unforseen
        # problems that we had with the hardware.
        self.__cs_regulator = CoffeeStrengthRegulator(io_pin=26,
                                                      default_value=0)
        # Initizalize the Coffee volume regulator,
        # Although this will not be used due to unforseen
        # problems that we had with the hardware.
        self.__cv_regulator = CoffeeVolumeRegulator(io_pin=13,
                                                    default_value=0,
                                                    is_pwm=True)

        # Initializing the 4 buttons that we can use to control the hardware
        # Initializing the power button regulator and turn it off by default.
        self.__power_button_regulator = Regulator(io_pin=power_io_pin)
        self.__power_button_regulator.off()
        # Initializing the coffee button regulator and turn it off by default.
        self.__coffee_button_regulator = Regulator(io_pin=coffee_io_pin)
        self.__coffee_button_regulator.off()
        # Initializing the strong coffee button regulator
        # and turn it off by default.
        self.__scb_regulator = Regulator(io_pin=strong_coffee_io_pin)
        self.__scb_regulator.off()
        # Initializing the cleaning button regulator
        # and turn it off by default.
        self.__cleaning_button_regulator = Regulator(io_pin=cleaning_io_pin)
        self.__cleaning_button_regulator.off()

        # Initialize the platform rotator
        self.__platform_rotator = PlatformRotator(4, 17, 18)

        # Initialize the AD Converter and add the sensors to it.
        self.__ad_converter = ADConverter()
        self.__boiler_sensor = TemperatureSensor(1, 2)
        self.__light_sensor = ADSensor(0, 2)
        self.__ad_converter.add_sensor(self.__boiler_sensor)
        self.__ad_converter.add_sensor(self.__light_sensor)

        # Start the continuous loop,
        # to start reading the values of the sensors
        self.__ad_converter.start_reading()

        # Initialize the UltrasoundSensor
        self.__water_height_sensor = UltraSoundSensor(5, 6)
        # Start the continuous loop,
        # to start reading the values of the sensors
        self.__water_height_sensor.start_reading()

        # Set current status of the coffeemachine to idle
        self.__status = status.IDLE

        # Set brewing target variable and set currently brewing cup variable
        self.__brewing_target = 0
        self.__currently_brewing_cup = 0
        self.__brewing_strong = False

    def toggle_power(self):
        # Sends a on and off signal to simulate
        # a button press to the power button
        self.__power_button_regulator.simulate_button_press()

    def toggle_coffee_button(self):
        # Sends a on and off signal to simulate
        # a button press to the coffee button
        self.__coffee_button_regulator.simulate_button_press()

    def start_brewing(self, cups=1, strong=False):
        # Send signal n amount of times where n is cups
        for i in range(cups):
            if not strong:
                # If strong is false, than simulate normal coffee button press
                self.__coffee_button_regulator.simulate_button_press()
            else:
                # Else strong is false,
                # than simulate strong coffee button press
                self.__scb_regulator.simulate_button_press()
            # Add a small delay, so the hardware has time to clean up if needed
            time.sleep(0.4)

    def get_coffee_strength_regulator(self):
        return self.__cs_regulator

    def get_coffee_volume_regulator(self):
        return self.__cv_regulator

    def get_boiler_temperature(self):
        return self.__boiler_sensor.temperature

    def get_light_sensor_value(self):
        return self.__light_sensor.volts

    def get_water_height(self):
        return self.__water_height_sensor.distance

    def turn_platform(self, steps=1):
        for i in range(steps):
            print("Turning platform")
            self.__platform_rotator.rotate()
            # Check each second to see if the platform is still rotating.
            # If we do not do this, then the loop would continue and
            # send a new rotate signal, even though it's still rotating
            while self.__platform_rotator.is_rotating():
                time.sleep(1)

            time.sleep(2)

    def set_status(self, status):
        self.__status = status

    def get_status(self):
        return self.__status

    def is_idle(self):
        return self.__status == status.IDLE

    def get_brewing_target(self):
        return self.__brewing_target

    def set_brewing_target(self, target):
        self.__brewing_target = target

    def get_currently_brewing_cup(self):
        return self.__currently_brewing_cup

    def set_currently_brewing_cup(self, cup):
        self.__currently_brewing_cup = cup

    def get_brewing_strong(self):
        return self.__brewing_strong

    def set_brewing_strong(self, strong):
        self.__brewing_strong = strong

# Initialize a global_machine so that we can access it from anywhere
global_machine = CoffeeMachine(23, 22, 25, 24)
