import RPi.GPIO as GPIO


class Receiver:
    def __init__(self,
                 io_pin,
                 with_callback=True,
                 callback=None,
                 falling=False,
                 pull_up=False):
        self.io_pin = io_pin

        GPIO.setmode(GPIO.BCM)
        if pull_up:
            GPIO.setup(self.io_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        else:
            GPIO.setup(self.io_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        if falling:
            type = GPIO.FALLING
        else:
            type = GPIO.RISING
        if with_callback:
            self.add_event_callback(callback, type=type)

    def event_callback(self, x):
        pass

    def get_current_value(self):
        return GPIO.input(self.io_pin)

    def add_event_callback(self, func, type=GPIO.RISING):
        if func is not None:
            GPIO.add_event_detect(self.io_pin,
                                  type,
                                  callback=func)
