from flask_api import FlaskAPI, request
import app.config as Config
from app.router import AppRouter
import os

__all__ = ['create_app']


def create_app(config=None, app_name=None):
    """ Creates a new Flask app"""

    # If the app_name is not given,
    # use the Project name from the default config
    if app_name is None:
        app_name = Config.DefaultConfig.PROJECT

    # Create the Flask app
    app = FlaskAPI(app_name)

    # Configure the app using the given config even if config=None
    configure_app(app, config)

    # Configure the error_handlers so users will get an appropriate response
    configure_error_handlers(app)

    # Initialize the main router
    home = AppRouter(app=app)
    home.init_routes()

    # Finally return the configured app
    return app


def configure_app(app, config):
    """Configure the application based on the config file given"""

    # if the config has value
    if config:
        # Set the config of app to the given config
        app.config.from_object(config)
        return

    # Else get the ENV environment, defaulted to LOCAL
    env = os.getenv('ENV', default='LOCAL')

    # And set the config of the app,
    # with the config that belongs to the env
    app.config.from_object(Config.get_config(env))


def configure_error_handlers(app):
    # For error 500
    @app.errorhandler(500)
    def error_500(error):
        # return an object with status 500 and the error message as message
        return {'status': '500', 'message': error.message}
