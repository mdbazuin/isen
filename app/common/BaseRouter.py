class BaseRouter:

    # Used to initialize all children
    child_routers = []

    # Set the relative path for the class
    relative_path = '/'

    def __init__(self, app=None, path=None, parent=None):
        """Creates a new router"""

        # If the parent has been set,
        # And BaseRouter is not one of the subclasses of the parent class
        # Raise an exception
        if parent is not None and not issubclass(parent.__class__, BaseRouter):
            raise BaseException("Parent is not a subclass of BaseRouter")

        # create variable that we will alter under certain conditions
        if path:
            base_path = path
        else:
            base_path = self.relative_path

        # if the base_path is ending in a / we want to remove it
        # unless the length of the base_path is 1
        if base_path.endswith('/') and len(base_path) > 1:
            base_path[:len(base_path)-1]

        # If the parent parameter is given
        if parent is not None:
            # we want to add the parent's base path in front
            # of the current base_path.
            # Also make sure that all double slashes are replaced with
            # single slashes.
            base_path = (parent.base_path + base_path).replace('//', '/')
            # Set the app variable to the parent's app variable
            self.app = parent.app
        elif app is not None:
            # Set the app if the app parameter is given
            # and the parent parameter isn't given
            self.app = app

        # Set the base_path
        self.base_path = base_path

        # Set the parent
        self.parent = parent

        # Set children as list
        self.children = []

        # Initialize all children
        self.init_children()

    def routes(self):
        pass

    def init_children(self):
        """Initializes all children for the object"""
        for child_router in self.child_routers:
            # For each child router, create a new instance
            # and add them to the children list
            self.children.append(child_router(parent=self))

    def init_routes(self):
        # Add own routes first
        self.routes()

        # Then for each child, initialize the routes
        for child in self.children:
            child.init_routes()
