import os


class BaseConfig:
    """Base class for all Configs."""

    # Variable for the project name
    PROJECT = "app"

    # Get the absolute root path of the project
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

    # Debugging flag for enabling debug mode in FlaskAPI
    DEBUG = False

    # Testing flag for enabling testing
    TESTING = False


class DefaultConfig(BaseConfig):

    # Enable debugging
    DEBUG = True


class LocalConfig(DefaultConfig):
    pass


class StagingConfig(DefaultConfig):

    # Disable debugging in Staging
    DEBUG = False


class ProductionConfig(StagingConfig):
    pass


def get_config(env):
    configs = {
        'LOCAL': LocalConfig,
        'STAGING': StagingConfig,
        'PRODUCTION': ProductionConfig
    }
    return configs[env]
