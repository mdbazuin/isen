from app import create_app
import app.config as Config
import unittest


class TestAppCreateApp(unittest.TestCase):
    def test_create_app(self):
        subject = create_app()
        self.assertIsNotNone(subject)
        self.assertEqual(Config.DefaultConfig.PROJECT, subject.name)

    def test_create_app_given_app_name(self):
        given_app_name = 'TEST NAME FOR UNIT TEST'
        subject = create_app(app_name=given_app_name)
        self.assertIsNotNone(subject)
        self.assertEqual(given_app_name, subject.name)
