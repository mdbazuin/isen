import app.config as Config
import unittest


class TestConfig(unittest.TestCase):
    def test_get_config_local(self):
        self.assertEqual(Config.LocalConfig, Config.get_config('LOCAL'))

    def test_get_config_staging(self):
        self.assertEqual(Config.StagingConfig, Config.get_config('STAGING'))

    def test_get_config_production(self):
        self.assertEqual(Config.ProductionConfig,
                         Config.get_config('PRODUCTION'))


